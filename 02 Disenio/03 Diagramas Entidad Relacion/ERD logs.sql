/* ---------------------------------------------------- */
/*  Generated by Enterprise Architect Version 13.5 		*/
/*  Created On : 15-Ene-2018 05:12:25 p.m. 				*/
/*  DBMS       : SQL Server 2012 						*/
/* ---------------------------------------------------- */

/* Drop Tables */

IF EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = object_id(N'[AplBitacoraSistema]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1) 
DROP TABLE [AplBitacoraSistema]
GO

IF EXISTS (SELECT 1 FROM dbo.sysobjects WHERE id = object_id(N'[DatBitacoraUsuario]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1) 
DROP TABLE [DatBitacoraUsuario]
GO

/* Create Tables */

CREATE TABLE [AplBitacoraSistema]
(
	[idBitacoraSistema] bigint NOT NULL IDENTITY (1, 1),
	[idUsuario] bigint NULL,
	[idRolUsuario] smallint NOT NULL DEFAULT 1,
	[dated] datetime NOT NULL,
	[remoteIP] varchar(50) NULL,
	[levelMessage] varchar(10) NOT NULL,
	[logger] varchar(255) NOT NULL,
	[message] varchar(max) NOT NULL
)
GO

CREATE TABLE [DatBitacoraUsuario]
(
	[idBitacoraUsuario] bigint NOT NULL IDENTITY (1, 1),
	[fechaOperacion] datetime NOT NULL,
	[idUsuario] bigint NOT NULL,
	[idRolUsuario] smallint NOT NULL DEFAULT 1,
	[remoteIP] varchar(50) NOT NULL,
	[mensaje] varchar(max) NOT NULL,
	[operacionExitosa] tinyint NOT NULL
)
GO

/* Create Primary Keys, Indexes, Uniques, Checks */

ALTER TABLE [AplBitacoraSistema] 
 ADD CONSTRAINT [PK_AplBitacoraSistema]
	PRIMARY KEY CLUSTERED ([idBitacoraSistema] ASC)
GO

ALTER TABLE [DatBitacoraUsuario] 
 ADD CONSTRAINT [PK_DatBitacoraUsuario]
	PRIMARY KEY CLUSTERED ([idBitacoraUsuario] ASC)
GO
