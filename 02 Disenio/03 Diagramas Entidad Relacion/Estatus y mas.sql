-- Estatus del alumno:
-- EstatusUsuario: Solo el estatus Liquidado no aplica en el alumno
/*
------------------------------------------------------------------------------------------
EstatusSituacionAlumno|         EstatusPago             | CategoriaAlumno  | EstatusUsuario |
----------------------|---------------------------------|------------------|-----------------
Inscrito              | Pagado                          | Sobresaliente    | Activo
Reinscrito            | Pendiente pago / Cartera vencida| ...              | Liquidado
Repitecurso           | Pago parcial                    | ...              | Reactivado
Inscrito/adeudoDoc.
Inscrito condicionado por pago
Inscrito/adeudoDoc y pago
Reinscrito/adeudoDoc.
Reinscrito condicionado por pago
Reinscrito/adeudoDoc/adeudoPago

Las Generaciones se armarán en el momento en que finaliza el alumno con sus asignaturas tomando 
PeriodoTiempocomo referencia el año que ingresa (PeriodoTiempo) y el año en el que termina sus asignaturas (PeriodoTiempo)
*/

/*
Datos del ticket
	El nombre del alumno
    NumeroTiquet
*/
