-- MariaDB dump 10.19  Distrib 10.4.32-MariaDB, for Win64 (AMD64)
--
-- Host: localhost    Database: CEUM
-- ------------------------------------------------------
-- Server version	10.4.32-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Abecedario`
--

DROP TABLE IF EXISTS `Abecedario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Abecedario` (
  `idAbecedario` smallint(6) NOT NULL AUTO_INCREMENT,
  `letraAbecedario` varchar(10) NOT NULL,
  PRIMARY KEY (`idAbecedario`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251 COLLATE=cp1251_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Abecedario`
--

LOCK TABLES `Abecedario` WRITE;
/*!40000 ALTER TABLE `Abecedario` DISABLE KEYS */;
/*!40000 ALTER TABLE `Abecedario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Ciclo`
--

DROP TABLE IF EXISTS `Ciclo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Ciclo` (
  `idCiclo` smallint(6) NOT NULL AUTO_INCREMENT,
  `idNivelEducativo` smallint(6) NOT NULL,
  `nombreCiclo` varchar(45) NOT NULL,
  `numeroCiclo` smallint(6) NOT NULL,
  `fechaRegistro` date NOT NULL,
  `createDate` datetime NOT NULL,
  `recordDate` datetime NOT NULL,
  `idUsuarioCreate` bigint(20) NOT NULL,
  `idUsuarioRecord` bigint(20) NOT NULL,
  PRIMARY KEY (`idCiclo`),
  KEY `fk_Ciclo_NivelEducativo_idx` (`idNivelEducativo`),
  CONSTRAINT `fk_Ciclo_NivelEducativo` FOREIGN KEY (`idNivelEducativo`) REFERENCES `NivelEducativo` (`idNivelEducativo`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=cp1251 COLLATE=cp1251_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Ciclo`
--

LOCK TABLES `Ciclo` WRITE;
/*!40000 ALTER TABLE `Ciclo` DISABLE KEYS */;
/*!40000 ALTER TABLE `Ciclo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CicloTiempo`
--

DROP TABLE IF EXISTS `CicloTiempo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CicloTiempo` (
  `idCicloTiempo` smallint(6) NOT NULL AUTO_INCREMENT,
  `idTipoPeriodo` smallint(6) NOT NULL,
  `idMesInicio` smallint(6) NOT NULL,
  `idMesFinal` smallint(6) NOT NULL,
  `anioInicio` smallint(6) NOT NULL,
  `anioFinal` smallint(6) NOT NULL,
  `nombreCicloTiempo` varchar(45) NOT NULL,
  `createDate` datetime NOT NULL,
  `recordDate` datetime NOT NULL,
  `idUsuarioCreate` bigint(20) NOT NULL,
  `idUsuarioRecord` bigint(20) NOT NULL,
  PRIMARY KEY (`idCicloTiempo`),
  KEY `fk_CicloTiempo_Tipoperiodo_idx` (`idTipoPeriodo`),
  KEY `fk_CicloTiempo_Mes_idx` (`idMesInicio`),
  KEY `fk_CicloTiempo_Mes_idx1` (`idMesFinal`),
  CONSTRAINT `fk_CicloTiempo_Mes` FOREIGN KEY (`idMesInicio`) REFERENCES `Mes` (`idMes`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_CicloTiempo_Tipoperiodo` FOREIGN KEY (`idTipoPeriodo`) REFERENCES `TipoPeriodo` (`idTipoPeriodo`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=cp1251 COLLATE=cp1251_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CicloTiempo`
--

LOCK TABLES `CicloTiempo` WRITE;
/*!40000 ALTER TABLE `CicloTiempo` DISABLE KEYS */;
/*!40000 ALTER TABLE `CicloTiempo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EstatusCatalogo`
--

DROP TABLE IF EXISTS `EstatusCatalogo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EstatusCatalogo` (
  `idEstatusCatalogo` smallint(6) NOT NULL AUTO_INCREMENT,
  `nombreEstatusCatalogo` varchar(45) NOT NULL,
  PRIMARY KEY (`idEstatusCatalogo`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=cp1251 COLLATE=cp1251_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EstatusCatalogo`
--

LOCK TABLES `EstatusCatalogo` WRITE;
/*!40000 ALTER TABLE `EstatusCatalogo` DISABLE KEYS */;
INSERT INTO `EstatusCatalogo` VALUES (1,'Activo'),(2,'Liquidacion');
/*!40000 ALTER TABLE `EstatusCatalogo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EstatusPago`
--

DROP TABLE IF EXISTS `EstatusPago`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EstatusPago` (
  `idEstatusPago` smallint(6) NOT NULL AUTO_INCREMENT,
  `nombreEstatusPago` varchar(150) NOT NULL,
  PRIMARY KEY (`idEstatusPago`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251 COLLATE=cp1251_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EstatusPago`
--

LOCK TABLES `EstatusPago` WRITE;
/*!40000 ALTER TABLE `EstatusPago` DISABLE KEYS */;
/*!40000 ALTER TABLE `EstatusPago` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EstatusUsuario`
--

DROP TABLE IF EXISTS `EstatusUsuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EstatusUsuario` (
  `idEstatusUsuario` smallint(6) NOT NULL AUTO_INCREMENT,
  `idEstatusCatalogo` smallint(6) NOT NULL,
  `nombreEstatusUsuario` varchar(45) NOT NULL,
  `descripcionEstatusUsuario` text DEFAULT NULL,
  `fechaRegistro` date NOT NULL,
  `fechaLiquidacion` date DEFAULT NULL,
  `createDate` datetime NOT NULL,
  `recordDate` datetime NOT NULL,
  `idUsuarioCreate` bigint(20) NOT NULL,
  `idUsuarioRecord` bigint(20) NOT NULL,
  PRIMARY KEY (`idEstatusUsuario`),
  KEY `fk_EstatusUsuario_EstatusCatalogo_idx` (`idEstatusCatalogo`),
  CONSTRAINT `fk_EstatusUsuario_EstatusCatalogo` FOREIGN KEY (`idEstatusCatalogo`) REFERENCES `EstatusCatalogo` (`idEstatusCatalogo`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=cp1251 COLLATE=cp1251_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EstatusUsuario`
--

LOCK TABLES `EstatusUsuario` WRITE;
/*!40000 ALTER TABLE `EstatusUsuario` DISABLE KEYS */;
INSERT INTO `EstatusUsuario` VALUES (1,1,'Activo','Estatus activo','0000-00-00','0000-00-00','0000-00-00 00:00:00','0000-00-00 00:00:00',1,1);
/*!40000 ALTER TABLE `EstatusUsuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Localidad`
--

DROP TABLE IF EXISTS `Localidad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Localidad` (
  `idLocalidad` int(11) NOT NULL AUTO_INCREMENT,
  `claveLocalidad` varchar(45) DEFAULT NULL,
  `nombreLocalidad` varchar(250) NOT NULL,
  PRIMARY KEY (`idLocalidad`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251 COLLATE=cp1251_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Localidad`
--

LOCK TABLES `Localidad` WRITE;
/*!40000 ALTER TABLE `Localidad` DISABLE KEYS */;
/*!40000 ALTER TABLE `Localidad` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Mes`
--

DROP TABLE IF EXISTS `Mes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Mes` (
  `idMes` smallint(6) NOT NULL,
  `nombreMes` varchar(45) NOT NULL,
  PRIMARY KEY (`idMes`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251 COLLATE=cp1251_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Mes`
--

LOCK TABLES `Mes` WRITE;
/*!40000 ALTER TABLE `Mes` DISABLE KEYS */;
/*!40000 ALTER TABLE `Mes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Modalidad`
--

DROP TABLE IF EXISTS `Modalidad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Modalidad` (
  `idModalidad` smallint(6) NOT NULL AUTO_INCREMENT,
  `idEstatusCatalogo` smallint(6) NOT NULL,
  `nombreModalidad` varchar(45) NOT NULL,
  `fechaRegistro` date NOT NULL,
  `fechaLiquidacion` date NOT NULL,
  `createDate` datetime NOT NULL,
  `recordDate` datetime NOT NULL,
  `idUsuarioCreate` bigint(20) NOT NULL,
  `idUsuarioRecord` bigint(20) NOT NULL,
  PRIMARY KEY (`idModalidad`),
  KEY `fk_Modalidad_EstatusCatalogo_idx` (`idEstatusCatalogo`),
  CONSTRAINT `fk_Modalidad_EstatusCatalogo` FOREIGN KEY (`idEstatusCatalogo`) REFERENCES `EstatusCatalogo` (`idEstatusCatalogo`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=cp1251 COLLATE=cp1251_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Modalidad`
--

LOCK TABLES `Modalidad` WRITE;
/*!40000 ALTER TABLE `Modalidad` DISABLE KEYS */;
/*!40000 ALTER TABLE `Modalidad` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Municipio`
--

DROP TABLE IF EXISTS `Municipio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Municipio` (
  `idMunicipio` smallint(6) NOT NULL AUTO_INCREMENT,
  `claveMunicipio` varchar(45) DEFAULT NULL,
  `nombreMunicipio` varchar(250) NOT NULL,
  PRIMARY KEY (`idMunicipio`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251 COLLATE=cp1251_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Municipio`
--

LOCK TABLES `Municipio` WRITE;
/*!40000 ALTER TABLE `Municipio` DISABLE KEYS */;
/*!40000 ALTER TABLE `Municipio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `NivelEducativo`
--

DROP TABLE IF EXISTS `NivelEducativo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `NivelEducativo` (
  `idNivelEducativo` smallint(6) NOT NULL,
  `nombreNivelEducativo` varchar(50) NOT NULL,
  `fechaRegistro` date NOT NULL,
  `createDate` datetime NOT NULL,
  `recordDate` datetime NOT NULL,
  `idUsuarioCreate` bigint(20) NOT NULL,
  `idUsuarioRecord` bigint(20) NOT NULL,
  PRIMARY KEY (`idNivelEducativo`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251 COLLATE=cp1251_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `NivelEducativo`
--

LOCK TABLES `NivelEducativo` WRITE;
/*!40000 ALTER TABLE `NivelEducativo` DISABLE KEYS */;
/*!40000 ALTER TABLE `NivelEducativo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `NumeroControl`
--

DROP TABLE IF EXISTS `NumeroControl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `NumeroControl` (
  `idNumeroControl` bigint(20) NOT NULL AUTO_INCREMENT,
  `idEstatusCatalogo` smallint(6) NOT NULL,
  `idAbecedario` smallint(6) NOT NULL,
  `anio` smallint(6) NOT NULL,
  `consecutivo` varchar(45) NOT NULL,
  `letraGrupo` varchar(45) DEFAULT NULL,
  `numeroControl` varchar(45) NOT NULL,
  `fechaRegistro` date NOT NULL,
  `fechaLiquidacion` date DEFAULT NULL,
  `createDate` datetime NOT NULL,
  `recordDate` datetime NOT NULL,
  `idUsuarioCreate` bigint(20) NOT NULL,
  `idUsuarioRecord` bigint(20) NOT NULL,
  PRIMARY KEY (`idNumeroControl`),
  KEY `fk_NumeroControl_EstatusCatalogo_idx` (`idEstatusCatalogo`),
  KEY `fk_NumeroControl_Abecedario_idx` (`idAbecedario`),
  CONSTRAINT `fk_NumeroControl_Abecedario` FOREIGN KEY (`idAbecedario`) REFERENCES `Abecedario` (`idAbecedario`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_NumeroControl_EstatusCatalogo` FOREIGN KEY (`idEstatusCatalogo`) REFERENCES `EstatusCatalogo` (`idEstatusCatalogo`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=cp1251 COLLATE=cp1251_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `NumeroControl`
--

LOCK TABLES `NumeroControl` WRITE;
/*!40000 ALTER TABLE `NumeroControl` DISABLE KEYS */;
/*!40000 ALTER TABLE `NumeroControl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `OfertaAcademica`
--

DROP TABLE IF EXISTS `OfertaAcademica`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `OfertaAcademica` (
  `idOfertaAcademica` smallint(6) NOT NULL AUTO_INCREMENT,
  `idOfertaEducativa` smallint(6) NOT NULL,
  `idEstatusCatalogo` smallint(6) NOT NULL,
  `claveOfertaAcademica` varchar(45) DEFAULT NULL,
  `nombreOfertaAcademica` varchar(250) NOT NULL,
  `fechaRegistro` date NOT NULL,
  `fechaLiquidacion` date DEFAULT NULL,
  `createDate` datetime NOT NULL,
  `recordDate` datetime NOT NULL,
  `idUsuarioCreate` bigint(20) NOT NULL,
  `idUsuarioRecord` bigint(20) NOT NULL,
  PRIMARY KEY (`idOfertaAcademica`),
  KEY `fk_OfertaAcademica_OfertaEducativa_idx` (`idOfertaEducativa`),
  KEY `fk_OfertaAcademica_EstatusCatalogo_idx` (`idEstatusCatalogo`),
  CONSTRAINT `fk_OfertaAcademica_EstatusCatalogo` FOREIGN KEY (`idEstatusCatalogo`) REFERENCES `EstatusCatalogo` (`idEstatusCatalogo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_OfertaAcademica_OfertaEducativa` FOREIGN KEY (`idOfertaEducativa`) REFERENCES `OfertaEducativa` (`idOfertaEducativa`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=cp1251 COLLATE=cp1251_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `OfertaAcademica`
--

LOCK TABLES `OfertaAcademica` WRITE;
/*!40000 ALTER TABLE `OfertaAcademica` DISABLE KEYS */;
/*!40000 ALTER TABLE `OfertaAcademica` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `OfertaEducativa`
--

DROP TABLE IF EXISTS `OfertaEducativa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `OfertaEducativa` (
  `idOfertaEducativa` smallint(6) NOT NULL AUTO_INCREMENT,
  `idNivelEducativo` smallint(6) NOT NULL,
  `idEstatusCatalogo` smallint(6) NOT NULL,
  `nombreOfertaEducativa` varchar(100) NOT NULL,
  `claveOfertaEducativa` varchar(45) DEFAULT NULL,
  `fechaRegistro` date NOT NULL,
  `fechaLiquidacion` date DEFAULT NULL,
  `createDate` datetime NOT NULL,
  `recordDate` datetime NOT NULL,
  `idUsuarioCreate` bigint(20) NOT NULL,
  `idUsuarioRecord` bigint(20) NOT NULL,
  PRIMARY KEY (`idOfertaEducativa`),
  KEY `fk_OfertaEducativa_NivelEducativo_idx` (`idNivelEducativo`),
  KEY `fk_OfertaEducativa_EstatusCatalogo_idx` (`idEstatusCatalogo`),
  CONSTRAINT `fk_OfertaEducativa_EstatusCatalogo` FOREIGN KEY (`idEstatusCatalogo`) REFERENCES `EstatusCatalogo` (`idEstatusCatalogo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_OfertaEducativa_NivelEducativo` FOREIGN KEY (`idNivelEducativo`) REFERENCES `NivelEducativo` (`idNivelEducativo`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=cp1251 COLLATE=cp1251_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `OfertaEducativa`
--

LOCK TABLES `OfertaEducativa` WRITE;
/*!40000 ALTER TABLE `OfertaEducativa` DISABLE KEYS */;
/*!40000 ALTER TABLE `OfertaEducativa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `OportunidadAcreditacion`
--

DROP TABLE IF EXISTS `OportunidadAcreditacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `OportunidadAcreditacion` (
  `idOportunidadAcreditacion` smallint(6) NOT NULL AUTO_INCREMENT,
  `idNivelEducativo` smallint(6) NOT NULL,
  `idEstatusCatalogo` smallint(6) NOT NULL,
  `nombreOportunidadAcreditacion` varchar(100) NOT NULL,
  `claveOportunidadAcreditacion` varchar(45) DEFAULT NULL,
  `fechaRegistro` date NOT NULL,
  `fechaLiquidacion` date NOT NULL,
  `createDate` datetime NOT NULL,
  `recordDate` datetime NOT NULL,
  `idUsuarioCreate` bigint(20) NOT NULL,
  `idUsuarioRecord` bigint(20) NOT NULL,
  PRIMARY KEY (`idOportunidadAcreditacion`),
  KEY `fk_OportunidadAcreditacion_NivelEducativo_idx` (`idNivelEducativo`),
  KEY `fk_OportunidadAcreditacion_EstatusCatalogo_idx` (`idEstatusCatalogo`),
  CONSTRAINT `fk_OportunidadAcreditacion_EstatusCatalogo` FOREIGN KEY (`idEstatusCatalogo`) REFERENCES `EstatusCatalogo` (`idEstatusCatalogo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_OportunidadAcreditacion_NivelEducativo` FOREIGN KEY (`idNivelEducativo`) REFERENCES `NivelEducativo` (`idNivelEducativo`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=cp1251 COLLATE=cp1251_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `OportunidadAcreditacion`
--

LOCK TABLES `OportunidadAcreditacion` WRITE;
/*!40000 ALTER TABLE `OportunidadAcreditacion` DISABLE KEYS */;
/*!40000 ALTER TABLE `OportunidadAcreditacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Periodo`
--

DROP TABLE IF EXISTS `Periodo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Periodo` (
  `idPeriodo` int(11) NOT NULL AUTO_INCREMENT,
  `idTipoPeriodo` smallint(6) NOT NULL,
  `idCiclo` smallint(6) NOT NULL,
  `nombrePeriodo` varchar(45) NOT NULL,
  `numeroPeriodo` smallint(6) NOT NULL,
  `createDate` datetime NOT NULL,
  `recordDate` datetime NOT NULL,
  `idUsuarioCreate` bigint(20) NOT NULL,
  `idUsuarioRecord` bigint(20) NOT NULL,
  PRIMARY KEY (`idPeriodo`),
  KEY `fk_Periodo_TipoPeriodo_idx` (`idTipoPeriodo`),
  KEY `fk_Periodo_Ciclo_idx` (`idCiclo`),
  CONSTRAINT `fk_Periodo_Ciclo` FOREIGN KEY (`idCiclo`) REFERENCES `Ciclo` (`idCiclo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Periodo_TipoPeriodo` FOREIGN KEY (`idTipoPeriodo`) REFERENCES `TipoPeriodo` (`idTipoPeriodo`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=cp1251 COLLATE=cp1251_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Periodo`
--

LOCK TABLES `Periodo` WRITE;
/*!40000 ALTER TABLE `Periodo` DISABLE KEYS */;
/*!40000 ALTER TABLE `Periodo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PeriodoTiempo`
--

DROP TABLE IF EXISTS `PeriodoTiempo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PeriodoTiempo` (
  `idPeriodoTiempo` int(11) NOT NULL AUTO_INCREMENT,
  `idCicloTiempo` smallint(6) NOT NULL,
  `idMesInicio` smallint(6) NOT NULL,
  `idMesFinal` smallint(6) NOT NULL,
  `anioInicio` smallint(6) NOT NULL,
  `anioFinal` smallint(6) NOT NULL,
  `nombrePeriodoTiempo` varchar(45) NOT NULL,
  `createDate` datetime NOT NULL,
  `recordDate` datetime NOT NULL,
  `idUsuarioCreate` bigint(20) NOT NULL,
  `idUsuarioRecord` bigint(20) NOT NULL,
  PRIMARY KEY (`idPeriodoTiempo`),
  KEY `fk_PeriodoTiempo_CicloTiempo_idx` (`idCicloTiempo`),
  KEY `fk_PeriodoTiempo_Mes_idx` (`idMesInicio`),
  KEY `fk_PeriodoTiempo_MesF_idx` (`idMesFinal`),
  CONSTRAINT `fk_PeriodoTiempo_CicloTiempo` FOREIGN KEY (`idCicloTiempo`) REFERENCES `CicloTiempo` (`idCicloTiempo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_PeriodoTiempo_Mes` FOREIGN KEY (`idMesInicio`) REFERENCES `Mes` (`idMes`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_PeriodoTiempo_MesF` FOREIGN KEY (`idMesFinal`) REFERENCES `Mes` (`idMes`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=cp1251 COLLATE=cp1251_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PeriodoTiempo`
--

LOCK TABLES `PeriodoTiempo` WRITE;
/*!40000 ALTER TABLE `PeriodoTiempo` DISABLE KEYS */;
/*!40000 ALTER TABLE `PeriodoTiempo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PlanEstudio`
--

DROP TABLE IF EXISTS `PlanEstudio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PlanEstudio` (
  `idPlanEstudio` smallint(6) NOT NULL AUTO_INCREMENT,
  `idOfertaAcademica` smallint(6) NOT NULL,
  `idModalidad` smallint(6) NOT NULL,
  `idTipoPeriodo` smallint(6) NOT NULL,
  `idEstatusCatalogo` smallint(6) NOT NULL,
  `nombrePlanEstudio` varchar(250) NOT NULL,
  `clavePlanEstudio` varchar(45) DEFAULT NULL,
  `descripcionPlanEstudio` text DEFAULT NULL,
  `fechaRegistro` date NOT NULL,
  `fechaLiquidacion` date DEFAULT NULL,
  `anioRegistro` varchar(4) NOT NULL,
  `archivoAnexo` varchar(250) DEFAULT NULL,
  `totalHoras` smallint(6) NOT NULL,
  `totalCreditos` smallint(6) NOT NULL,
  `createDate` datetime NOT NULL,
  `recordDate` datetime NOT NULL,
  `idUsuarioCreate` bigint(20) NOT NULL,
  `idUsuarioRecord` bigint(20) NOT NULL,
  PRIMARY KEY (`idPlanEstudio`),
  KEY `fk_PlanEstudio_OfertaAcademica_idx` (`idOfertaAcademica`),
  KEY `fk_PlanEstudio_Modalidad_idx` (`idModalidad`),
  KEY `fk_PlanEstudio_TipoPeriodo_idx` (`idTipoPeriodo`),
  KEY `fk_PlanEstudio_EstatusCatalogo_idx` (`idEstatusCatalogo`),
  CONSTRAINT `fk_PlanEstudio_EstatusCatalogo` FOREIGN KEY (`idEstatusCatalogo`) REFERENCES `EstatusCatalogo` (`idEstatusCatalogo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_PlanEstudio_Modalidad` FOREIGN KEY (`idModalidad`) REFERENCES `Modalidad` (`idModalidad`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_PlanEstudio_OfertaAcademica` FOREIGN KEY (`idOfertaAcademica`) REFERENCES `OfertaAcademica` (`idOfertaAcademica`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_PlanEstudio_TipoPeriodo` FOREIGN KEY (`idTipoPeriodo`) REFERENCES `TipoPeriodo` (`idTipoPeriodo`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=cp1251 COLLATE=cp1251_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PlanEstudio`
--

LOCK TABLES `PlanEstudio` WRITE;
/*!40000 ALTER TABLE `PlanEstudio` DISABLE KEYS */;
/*!40000 ALTER TABLE `PlanEstudio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `RolUsuario`
--

DROP TABLE IF EXISTS `RolUsuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RolUsuario` (
  `idRolUsuario` smallint(6) NOT NULL AUTO_INCREMENT,
  `nombreRolUsuario` varchar(45) NOT NULL,
  PRIMARY KEY (`idRolUsuario`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=cp1251 COLLATE=cp1251_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `RolUsuario`
--

LOCK TABLES `RolUsuario` WRITE;
/*!40000 ALTER TABLE `RolUsuario` DISABLE KEYS */;
INSERT INTO `RolUsuario` VALUES (1,'Super administrador'),(2,'Director'),(3,'Director academico'),(4,'Director administrativo'),(5,'Servicios escolares'),(6,'Cobranza'),(7,'Promocion'),(8,'Docente'),(9,'Alumno');
/*!40000 ALTER TABLE `RolUsuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Sexo`
--

DROP TABLE IF EXISTS `Sexo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Sexo` (
  `idSexo` smallint(6) NOT NULL AUTO_INCREMENT,
  `nombreSexo` varchar(45) NOT NULL,
  PRIMARY KEY (`idSexo`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=cp1251 COLLATE=cp1251_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Sexo`
--

LOCK TABLES `Sexo` WRITE;
/*!40000 ALTER TABLE `Sexo` DISABLE KEYS */;
INSERT INTO `Sexo` VALUES (1,'Masculino'),(2,'Femenino'),(3,'Otro');
/*!40000 ALTER TABLE `Sexo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `TabuladorServiciosEscolares`
--

DROP TABLE IF EXISTS `TabuladorServiciosEscolares`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TabuladorServiciosEscolares` (
  `idTabuladorServiciosEscolares` smallint(6) NOT NULL AUTO_INCREMENT,
  `idEstatusCatalogo` smallint(6) NOT NULL,
  `nombreTabuladorServiciosEscolares` varchar(45) NOT NULL,
  `descripcionTabuladorServiciosEscolares` text DEFAULT NULL,
  `fechaRegistro` date NOT NULL,
  `fechaLiquidacion` date DEFAULT NULL,
  `createDate` datetime NOT NULL,
  `recordDate` datetime NOT NULL,
  `idUsuarioCreate` bigint(20) NOT NULL,
  `idUsuarioRecord` bigint(20) NOT NULL,
  PRIMARY KEY (`idTabuladorServiciosEscolares`),
  KEY `fk_TabuladorServiciosEscolares_EstatusCatalogo_idx` (`idEstatusCatalogo`),
  CONSTRAINT `fk_TabuladorServiciosEscolares_EstatusCatalogo` FOREIGN KEY (`idEstatusCatalogo`) REFERENCES `EstatusCatalogo` (`idEstatusCatalogo`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=cp1251 COLLATE=cp1251_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `TabuladorServiciosEscolares`
--

LOCK TABLES `TabuladorServiciosEscolares` WRITE;
/*!40000 ALTER TABLE `TabuladorServiciosEscolares` DISABLE KEYS */;
/*!40000 ALTER TABLE `TabuladorServiciosEscolares` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `TipoAsignatura`
--

DROP TABLE IF EXISTS `TipoAsignatura`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TipoAsignatura` (
  `idTipoAsignatura` smallint(6) NOT NULL AUTO_INCREMENT,
  `nombreTipoAsignatura` varchar(45) NOT NULL,
  PRIMARY KEY (`idTipoAsignatura`)
) ENGINE=InnoDB DEFAULT CHARSET=cp1251 COLLATE=cp1251_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `TipoAsignatura`
--

LOCK TABLES `TipoAsignatura` WRITE;
/*!40000 ALTER TABLE `TipoAsignatura` DISABLE KEYS */;
/*!40000 ALTER TABLE `TipoAsignatura` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `TipoPeriodo`
--

DROP TABLE IF EXISTS `TipoPeriodo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TipoPeriodo` (
  `idTipoPeriodo` smallint(6) NOT NULL AUTO_INCREMENT,
  `idEstatusCatalogo` smallint(6) NOT NULL,
  `nombreTipoPeriodo` varchar(45) NOT NULL,
  `descripcionTipoPeriodo` text DEFAULT NULL,
  `fechaRegistro` date NOT NULL,
  `fechaLiguidacion` date DEFAULT NULL,
  `createDate` datetime NOT NULL,
  `recordDate` datetime NOT NULL,
  `idUsuarioCreate` bigint(20) NOT NULL,
  `idUsuarioRecord` bigint(20) NOT NULL,
  PRIMARY KEY (`idTipoPeriodo`),
  KEY `fk_TipoPeriodo_EstatusCatalogo_idx` (`idEstatusCatalogo`),
  CONSTRAINT `fk_TipoPeriodo_EstatusCatalogo` FOREIGN KEY (`idEstatusCatalogo`) REFERENCES `EstatusCatalogo` (`idEstatusCatalogo`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=cp1251 COLLATE=cp1251_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `TipoPeriodo`
--

LOCK TABLES `TipoPeriodo` WRITE;
/*!40000 ALTER TABLE `TipoPeriodo` DISABLE KEYS */;
/*!40000 ALTER TABLE `TipoPeriodo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `TokenContrasenia`
--

DROP TABLE IF EXISTS `TokenContrasenia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TokenContrasenia` (
  `idTokenContrasenia` bigint(20) NOT NULL AUTO_INCREMENT,
  `idUsuario` bigint(20) NOT NULL,
  `token` varchar(250) NOT NULL,
  `createDate` datetime NOT NULL,
  `recordDate` datetime NOT NULL,
  `idUsuarioCreate` bigint(20) NOT NULL,
  `idUsuarioRecord` bigint(20) NOT NULL,
  PRIMARY KEY (`idTokenContrasenia`),
  KEY `fk_Usuario_TokenUsuario_idx` (`idUsuario`),
  CONSTRAINT `fk_Usuario_TokenUsuario` FOREIGN KEY (`idUsuario`) REFERENCES `Usuario` (`idUsuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=cp1251 COLLATE=cp1251_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `TokenContrasenia`
--

LOCK TABLES `TokenContrasenia` WRITE;
/*!40000 ALTER TABLE `TokenContrasenia` DISABLE KEYS */;
/*!40000 ALTER TABLE `TokenContrasenia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Turno`
--

DROP TABLE IF EXISTS `Turno`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Turno` (
  `idTurno` smallint(6) NOT NULL AUTO_INCREMENT,
  `idEstatusCatalogo` smallint(6) NOT NULL,
  `nombreTurno` varchar(45) NOT NULL,
  `fechaRegistro` date NOT NULL,
  `fechaLiquidacion` date DEFAULT NULL,
  `createDate` datetime NOT NULL,
  `recordDate` datetime NOT NULL,
  `idUsuarioCreate` bigint(20) NOT NULL,
  `idUsuarioRecord` bigint(20) NOT NULL,
  PRIMARY KEY (`idTurno`),
  KEY `fk_Turno_EstatusCatalogo_idx` (`idEstatusCatalogo`),
  CONSTRAINT `fk_Turno_EstatusCatalogo` FOREIGN KEY (`idEstatusCatalogo`) REFERENCES `EstatusCatalogo` (`idEstatusCatalogo`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=cp1251 COLLATE=cp1251_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Turno`
--

LOCK TABLES `Turno` WRITE;
/*!40000 ALTER TABLE `Turno` DISABLE KEYS */;
/*!40000 ALTER TABLE `Turno` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Usuario`
--

DROP TABLE IF EXISTS `Usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Usuario` (
  `idUsuario` bigint(20) NOT NULL AUTO_INCREMENT,
  `idSexo` smallint(6) NOT NULL,
  `idEstatusUsuario` smallint(6) NOT NULL,
  `idRolUsuario` smallint(6) NOT NULL,
  `nombreUsuario` varchar(250) NOT NULL,
  `apPaternoUsuario` varchar(250) NOT NULL,
  `apMaternoUsuario` varchar(250) DEFAULT NULL,
  `curpUsuario` varchar(45) NOT NULL,
  `rfcUsuario` varchar(45) DEFAULT NULL,
  `fechaNacimiento` date NOT NULL,
  `telefonoUsuario` varchar(15) NOT NULL,
  `urlFotoUsuario` varchar(250) DEFAULT NULL,
  `usuario` varchar(250) NOT NULL,
  `contrasenia` varchar(40) NOT NULL,
  `fechaRegistro` date NOT NULL,
  `fechaBaja` date NOT NULL,
  `fechaReactivacion` date DEFAULT NULL,
  `createDate` datetime NOT NULL,
  `recordDate` datetime NOT NULL,
  `idUsuarioCreate` bigint(20) NOT NULL,
  `idUsuarioRecord` bigint(20) NOT NULL,
  PRIMARY KEY (`idUsuario`),
  KEY `fk_Usuario_Sexo_idx` (`idSexo`),
  KEY `fk_Usuario_EstatusUsuario_idx` (`idEstatusUsuario`),
  KEY `fk_Usuario_RolUsuario_idx` (`idRolUsuario`),
  CONSTRAINT `fk_Usuario_EstatusUsuario` FOREIGN KEY (`idEstatusUsuario`) REFERENCES `EstatusUsuario` (`idEstatusUsuario`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Usuario_RolUsuario` FOREIGN KEY (`idRolUsuario`) REFERENCES `RolUsuario` (`idRolUsuario`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Usuario_Sexo` FOREIGN KEY (`idSexo`) REFERENCES `Sexo` (`idSexo`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=cp1251 COLLATE=cp1251_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Usuario`
--

LOCK TABLES `Usuario` WRITE;
/*!40000 ALTER TABLE `Usuario` DISABLE KEYS */;
INSERT INTO `Usuario` VALUES (1,1,1,4,'Prueba','Prueba','','XXAD010101XXSRXX01','XXAD010101K31','1992-04-23','5555555555',NULL,'dadministrativo@ceum.mx','d41d8cd98f00b204e9800998ecf8427e','2024-04-04','0000-00-00',NULL,'2024-04-04 22:00:11','2024-04-04 22:00:11',1,1);
/*!40000 ALTER TABLE `Usuario` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-04-05 13:58:27
